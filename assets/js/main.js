function open(part){

	var title = part.getElementsByTagName("h2")[0];

	title.addEventListener("click", function(){

		var par = this.parentElement.querySelector(".part");

		if(par.classList.contains("closed")){

			par.classList.remove("closed");
		
		}else{

			par.classList.add("closed");

		}

	});
}


function parts(){
	
	var parts = document.querySelectorAll(".talk section");

	if(parts){

		for(var i=0; i<parts.length; i++){

			open(parts[i]);
		}
	}
}

document.addEventListener("DOMContentLoaded", function(){

	parts();

});
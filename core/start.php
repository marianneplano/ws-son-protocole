<?php

/* LIBRAIRIES */

require "vendor/autoload.php";

/* CLASS */

require "core/class/datas.php";
require "core/class/routes.php";
require "core/class/page.php";
require "core/class/pad.php";

/* VARS */

$pad = new Pad;
$datas = new Datas;
$page = new Page;

$cours = $datas->loadFiles("programmer", true);
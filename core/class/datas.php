<?php

class Datas{

	/* TEXT FORMAT */

	public function loadJson($path){

		return json_decode(file_get_contents($path));
	}

	public function loadMd($path){

		return $this->parse(file_get_contents($path));
	}

	public function parse($md){

		$parse = new Parsedown;
		$parse->setSafeMode(true);

		return $parse->text($md);
	}

	/* FILE SYSTEM */

	public function setObject($file){

		$item = new stdClass;
		$item->content = $this->loadMd($file);
		$item->id = pathinfo($file)["filename"];

		return $item;
	}

	public function loadFiles($name, $isFolder=false){

		$files = ($isFolder == true) ? glob("content/".$name."/*") : "content/".$name.".md";

		if(is_array($files)){

			$array = [];

			foreach($files as $file){

				$array [] = $this->setObject($file);
			}

			return $array;

		}else{

			return $this->setObject($file);
		}
	}

	public function write($source, $destination){

		$newFile =  file_get_contents($source);

		if(!file_exists($destination)){

			file_put_contents($destination, $newFile);

		}else{

			$oldFile = file_get_contents($destination);

			if($oldFile !== $newFile){

				file_put_contents($destination, $newFile);

			}

		}

	}

}
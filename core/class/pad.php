<?php

class Pad extends Routes{

	private function id(){

		return $this->loadJson("access/pad.json")->id;
	}

	private function url(){

		return "https://annuel.framapad.org/p/".$this->id();
	}

	public function localPad(){

		return "content/pad.md";
	}


	public function load(){

		$this->parse(file_get_contents($this->localPad()));
	}

	public function refresh(){

		$pad = $this->url()."/export/txt";
		$dest = $this->localPad();

		$this->write($pad, $dest);
		$this->redirect($this->home());
	}
}
<?php

class Page extends Routes{

	public function current(){

		$page = explode(".", basename($_SERVER['PHP_SELF']))[0];

		return ($page == "index") ? "home" : $page;
	}

	public function isPage($page){

		$current = $this->current();

		return ($current == $page);
	}


}
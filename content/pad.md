## Consigne

Création d’un instrument sonore ainsi que de sa partition dans le but d’aboutir à une performance réalisée par un interprète.

### 1 >>> L’instrument

1. Objet sonore
2. Installation sonore
3. Diffusion sonore

### 2 >>> La partition

1. Protocole
2. Partition graphique
3. Notes de mise en scène

### 3 >>> La performance

1. Choix de l’intérprète
2. Durée minimum: 2’00’’
3. Choix de l’espace et intégration
4. Intégration du public

## Programme

### JE 28.11

9h00–10h30
* Présentation du projet, consigne,
* Présentation des références

10h30–16h00
* Recherches, références,
* Croquis, expérimentations. + RDV Individuels

### ME 04.12

0900–12h00
* RDV individuel ou par groupe
* Groupes par technique de travail?

13h00–16h00
* RDV individuel ou par groupe

### VE. 13.12

09h00–12h00
* Montage, installation/accrochage
et finalisation

13h00 > Prés. et critique Groupe 1

14h00 > Prés. et critique Groupe 2

16h00 > Prés. et critique Groupe 3

## Références

### (Son) Programmabilité / Protocole / Automation

* Félix Blume – Rumors from the Sea, Thailand Biennale 2018 https://vimeo.com/297546895
* Zimoun 1977 –  Suisse https://www.youtube.com/watch?v=gpe0frCZ3jc
* Zimoun 1977 –  Suisse – Site Specific, Knockdown Center, New York https://www.youtube.com/watch?v=dP78RLzs2cQ&t=47s
* John Cage – USA 1912–1992 – 4’33’’ https://www.youtube.com/watch?v=JTEFKFiXSx4
* Nicolas Bernier – Quebec CAN – Ensemble d’Oscillateur https://vimeo.com/228616947
* Vassilakis Panayotis Takis 1925–2019 – Grèce https://www.youtube.com/watch?v=zg0XxxgpoP4&t=16s
* Takis – Espace musical – Centre pompidou /Beaubourg 1981 https://www.youtube.com/watch?v=GZYc_a8gJQY
* Martin Messier – Quebec CAN Sewing Machine Orchestra https://vimeo.com/61401753
* Sculptures Musicales, automates https://www.youtube.com/watch?v=EaS8lFdhGvE
* Yuri Suzuki https://www.youtube.com/watch?v=zWJxcf7u3hU

### (Son) Interactivité

* Les frères Baschet – Ressort - L’instrumentariuem pédagogique https://www.youtube.com/watch?v=DxpiIvNSRvc
* Harry Bertoia 1915–1978 - sculpteur/designer italien https://www.youtube.com/watch?v=YfQ3624z36Q
* Nicolas Bernier – Quebec CAN – Boite https://vimeo.com/4699613

### Musique concrète

* François Bayle 1932- – France https://vimeo.com/229104540
* Pierre Henry 1927-1917 – France https://vimeo.com/230775519
* Pierre Schaeffer 1910-1995 – France – Etudes aux Chemins de Fer https://www.youtube.co/watch?v=N9pOq8u6-bA > 1ère oeuvre de musique concrète réalisée en 1948
* Une introduction à la musique concrète https://www.youtube.com/watch?v=J2vNNW60yY8&t=795s










* [Marianne Plano](http://marianneplano.net)
* Design graphique + programmation
* [Collectif Luuse](http://luuse.io) (Bruxelles, BE)  

### Pratique de Luuse et Marianne

1. Les outils
    * Inventer ses propres outils de design
    * S'affranchir des logiciels existants

2. Le code
    * Générer des objets graphiques en utilisant la programmation
    * Appliquer des principes de la programmation au design graphique  
    Ex: Séparer le contenu de sa mise en forme -> un même contenu peut donner plusieurs formes

3. L’économie
    * Tenter de s’inscrire dans un écosystème libre == utiliser le moins possible de logiciels propriétaires

4. L’état d’esprit
    * Essayer, expérimenter, souvent rater
    * Perdre du temps pour en gagner

### Quelques exemples

* *East London Cable*, Website, 2019  
![](images/elc/elc_01.png)
![](images/elc/elc_02.png)
![](images/elc/elc_03.png)

* *Artificial Gut Feeling*, Anna Zett, Divided Publishing, 2019  
![](images/anna-zett/anna-zett_screen_01.png)
![](images/anna-zett/anna-zett_screen_02.png)
![](images/anna-zett/anna-zett_01.jpg)
![](images/anna-zett/anna-zett_02.jpeg)
![](images/anna-zett/anna-zett_04.jpeg)
![](images/anna-zett/anna-zett_05.jpeg)

* *Bocto Festival*, Website + Catalogue, 2018  
![](images/bocto/bocto-site_01.png)
![](images/bocto/bocto_01.jpg)  
![](images/bocto/bocto_02.jpg)  
![](images/bocto/bocto_03.jpg)

* *Autopia*, Typographie, 2016  
![](images/autopia/autopia_01.png)
![](images/autopia/autopia_02.png)
![](images/autopia/autopia_03.png)


### Concepts

Nous allons aujourd'hui parler de 3 des concepts de base de la programmation, en vue d'inspirer l’écriture de vos futures partitions.

1. Les variables  
    Les variables sont des boîtes, dans lesquelles les programmeurs rangent des données. Celles-ci peuvent être fixes, par exemple un nombre ou une chaîne de caractères qui ne changent pas, ou, comme leur nom l'indique, variables, c'est à dire qu'elle évoluent au fur et à mesure de l'exécution du programme. Elle peuvent être les mêmes au départ ou différer selon l'utilisateur du programme et ses besoins.
2. Les boucles  
    Dans un programme, il arrive tout le temps qu'on ait besoin de répéter une action plusieurs fois. Au lieu de réécrire le morceau de code que l'on souhaite répéter des dizaines, voir des centaines de fois (parce-que la vie est courte), on a inventé une notation qui existe dans absolument tous les langages de programmation: la boucle. Celle-ci nous permet d'ordonner à l'ordinateur d'executer plusieurs fois une même action sans alourdir le programme, sans compliquer la lecture du code mais aussi minimise les chances de se tromper lorsque l'on recopie.
3. Les conditions   
    Elles permettent de modifier le cours du programme en fonction d'événements, sous la forme de « si...alors ».
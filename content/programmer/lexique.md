### Ordinateur

Un ordinateur est :
1. Une machine qui peut être programmée. Ce qui signifie qu’elle est capable de lire des programmes, ce qui induit, de prendre des décisions sans intervention humaine manuelle, selon ce qu’ils spécifient.
2. Une machine qui ne différencie pas conceptuellement programme et données. Ils sont tous deux codés sous la même forme (0 et 1), et lisibles indifféremment par la machine.

### Algorithme

Un algorithme est « une recette, une série de tâches en vue d’accomplir un calcul ou un résultat spécifique, comme les étapes nécessaires pour obtenir la racine carrée d’un nombre. » Comme le souligne Dominique Cardon, ce terme a une signification bien plus large qu’on ne le croit, et s’apparente en pratique à la recette de cuisine, en tant que « description d’une méthode par laquelle une tâche peut être accomplie ».

Et le grand intérêt de l’algorithme, c’est qu’il « est un procédé qui permet de résoudre un problème, sans avoir besoin d’inventer une solution à chaque fois ». C’est le concept, l’outil de base pour traiter l’information. Dans le domaine des mathématiques, un algorithme se traduit par une suite d’opérations qui permettent d’aboutir à un résultat. Une liste d’instructions, qui appliquée à A donnent B. Par exemple, la méthode qui consiste à poser les additions que nous apprenons petits à l’école est un algorithme. Il décompose un problème compliqué en une série d’étapes simples, tel qu’ajouter des nombres à 1 chiffre, l’un après l’autre, ce qui est beaucoup plus facile pour un être humain. Il remplit ainsi une double fonction : réduire l’effort, et la marge d’erreur quant au résultat final.

### Programme

Les termes de « programme » et « d’algorithme » englobent chacun des significations extrêmement proches, qui se distinguent par le contexte. Un algorithme est une recette générique, le « mécanisme conceptuel de calcul systématique » alors que le programme se définit comme « l’écriture précise de l’algorithme dans des langages appropriés [langages dit de programmation] » qui communiquent avec une machine en vue d’effectuer une action. Le programme est donc un algorithme mais pas l’inverse.

On doit le premier programme à une femme, la britannique Ada Lovelace (1815-1852), qui travailla conjointement avec le mathématicien Charles Babbge sur la machine analytique. Babbage la chargea de traduire un texte qui décrivait son fonctionnement vers le français, pour une publication dans un journal suisse. Ce faisant, elle l’annota personnellement, ajoutant des explications. L’un de ses appendices contient un algorithme détaillé, permettant de calculer des nombres de Bernoulli à l’aide de la machine. Rétrospectivement, il fut désigné comme le premier programme informatique. 

### Langage de programmation

Un ordinateur ne comprend que les 0 et les 1. Pour lui dicter des actions, il faut donc les décrire ainsi. C’est ce qu’on appelle le « machine code ». Mais pour les êtres humains, parler ce langage machine est un enfer. Pour se faciliter la tâche, ils ont donc inventé une nouvelle manière de communiquer avec elle, à mi-chemin entre eux deux : les langages de programmation. Ils se composent d’un ensemble de règles syntaxiques, qui décrivent des actions basiques réalisables par l’ordinateur. Ils permettent d’écrire des programmes en des termes compréhensibles par les humains. Il en existe des centaines, qui répondent tous à des besoins différents. Si la machine comprend leurs instructions, c’est grâce à un autre programme qui « traduit » les instructions en 0 et en 1 : le complier. 
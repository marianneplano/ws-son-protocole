<?php require "core/start.php" ?>
<?php include "snippets/header.php" ?>
<header>
	<h1>WS — Son & Protocole</h1>
	<h4>EDEHA Sierre</h4>
</header>
<main>
	<?php foreach($cours as $part): ?>
		<section id="<?= $part->id ?>">
			<h2><?= ucfirst($part->id) ?></h2>
			<div class="part closed" >
				<?= $part->content ?>
			</div>
		</section>	
	<?php endforeach ?>	
</main>
<?php include "snippets/footer.php" ?>
</body>
</html>